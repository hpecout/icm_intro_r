################################################################################
#                                                                              #
#                   Manipulation de données netCDF avec R                      #
#                             Hugues Pecout                                    #
#                              28/11/2023                                      #
#                                                                              #
################################################################################



##########----------------- INSTALLATION PACKAGES --------------------##########

# install.packages("ncdf4")
# install.packages("R.utils")
# install.packages("chron")
# install.packages("lattice")
# install.packages("RColorBrewer")
# install.packages("rnaturalearth")
# install.packages("terra")
# install.packages("sf")
# install.packages("mapsf")
# install.packages("mapview")
# install.packages("ggplot2")




##########---------------------- IMPORT DONNEES ----------------------##########

# URL : https://crudata.uea.ac.uk/cru/data/hrg/cru_ts_4.07/  - 	Main gridded data

# URL de téléchargement
source <- "https://crudata.uea.ac.uk/cru/data/hrg/cru_ts_4.07/cruts.2304141047.v4.07"

# Fichier à télécharger
zip_file <- "cru_ts4.07.2021.2022.pre.dat.nc.gz"

#### Variables disponibles : ####

# cld : Cloud cover    
# dtr : Diurnal temperature range   
# frs : Frost day frequency   
# pet : Potential Evapo-transpiration    
# pre : Precipitation    
# tmn : Monthly average daily minimum temperature   
# tmp : Daily mean temperature   
# tmx : Monthly average daily maximumtemperature   
# vap : Vapour pressure   
# wet : Wet day frequency 




##########-------------------- PREPARATION IMPORT --------------------##########

#  nom de fichier décompréssé
file <- substring(zip_file, 1,  nchar(zip_file)-3)

# Variable téléchargée
variable <- substring(zip_file, 22,  24)

# Construction de l'URL de téléchargement
url_download <- paste(source, variable, zip_file, sep="/")

# Construction URL du répertoire où le fichier compréssé sera enregistré
url_import <- paste("data", zip_file, sep="/")

# Construction URL du répertoire où le fichier décompréssé sera enregistré
url_data <- paste("data", file, sep="/")

# Création d'un repertoire "data" pour stocker les données téléchargées
dir.create("data")





##########---------------------- IMPORT DONNEES ----------------------##########

#### Téléchargement du fichier de données netCDF
download.file(url = url_download,
              destfile = url_import)


# Decompression du fichier téléchargé
library(R.utils)
gunzip(url_import, remove = FALSE, overwrite = TRUE)





##########------------------- LECTURE DES DONNEES --------------------##########

library(ncdf4)
my_netCDF_data <- nc_open(url_data)

# Objet particulier "ncdf4" (de type LIST)
class(my_netCDF_data)
my_netCDF_data[[1]]

# Affichage d'informations sur l'objet "ncdf4"
print(my_netCDF_data)





##########---------------- INTERROGATION DES DONNEES -----------------##########

# Récupération des coordonnées (longitude and latitude) de la grille
lon <- ncvar_get(my_netCDF_data, "lon")
lat <- ncvar_get(my_netCDF_data, "lat")


# Récupération de la variable temporelle (dimension z)
time <- ncvar_get(my_netCDF_data, "time")
time

tunits <- ncatt_get(my_netCDF_data, "time", "units")
tunits

# Récupération du nom intelligible de la variable
var_name <- ncatt_get(my_netCDF_data,variable, "long_name")

# Récupération de l'unité de mesure
dunits <- ncatt_get(my_netCDF_data,variable, "units")

# Récupération de la valeur assignée aux valeurs nulles
fillvalue <- ncatt_get(my_netCDF_data,variable, "_FillValue")

# Récupération des attributs globaux du jeu de données
title <- ncatt_get(my_netCDF_data, 0, "title")
institution <- ncatt_get(my_netCDF_data, 0, "institution")
datasource <- ncatt_get(my_netCDF_data, 0, "source")
references <- ncatt_get(my_netCDF_data, 0, "references")
history <- ncatt_get(my_netCDF_data, 0, "history")
Conventions <- ncatt_get(my_netCDF_data, 0, "Conventions")


# Récupération des valeurs de la variable téléchargée
my_data <- ncvar_get(my_netCDF_data, variable)

# my_data est un objet "array" (ensemble de matrice)
dim(my_data)




##########------ TRANSFORMATION DES DATES (variable temporelle) ------##########

# Transformation du nombre de jour depuis 01/01/1900 en date
dates <- as.POSIXct(time*24*60*60, origin = "1900-01-01 00:00:00 UTC", tz="UTC")

# Transformation du format de la date
dates2 <- format(as.Date(dates, format="%d/%m/%Y"),"%b %Y")





##########---------------- Affichage simple des données --------------##########

# Extraction des valeurs de précipation pour le premier mois du jeu de données
data_slice <- my_data[ , , 1]

class(my_data)
class(data_slice)

# Affichage rapide des précipations du premier mois
library(RColorBrewer)
image(lon,lat,data_slice, col=rev(brewer.pal(10,"RdBu")))




##########----------- Conversion Array (matrice) -> RASTER -----------##########

library(terra)
# Conversion en format RASTER (SpatRaster)
my_data_raster <-rast(my_data, crs="+proj=utm +zone=1", extent=ext(-90, 90, -180, 180))

# Affichage des précipitations pour chaque mois du jeu de données
plot(my_data_raster)

# Transposition du RASTER dans le bon sens
my_data_raster <- flip(t(my_data_raster), direction= "v")

# Affichage des précipitations pour chaque mois du jeu de ddonnées
plot(my_data_raster)

# Affichage des données d'un seul mois ? Exemple = 1
mois <- 1
extract <- my_data_raster[[mois]]
plot(extract, main = dates2[mois])


##########----------- Récupération d'un fond de carte pays -----------##########

library(rnaturalearth)
library(sf)

# Téléchargement d'un fond de carte pays
countries <- ne_download(scale = 110, type = "admin_0_countries", returnclass = "sf")

# Affichage du fond de carte
plot(countries)
plot(st_geometry(countries))





##########--------- Cartographie complète des précipitations ---------##########

# Récupération de la valeur maximum (précipitation max)
maxi <- max(minmax(my_data_raster))

# Boucle pour générer une carte par mois
for (i in 1:nlyr(my_data_raster)){

  extract <- my_data_raster[[i]]
  plot(extract, 
       main = paste0(dates2[i], " - ", var_name$value), 
       type="continuous", 
       range=c(1,maxi)) 
  
  plot(countries, 
       lwd = 0.2,
       col = NA, 
       border = "grey50", 
       add = TRUE)
}


##########------------ Cartographie complète pour un pays  -----------##########

# Séléction polygone du MALI
ISO_country <- "MLI"
zoom_country <- countries[countries$ADM0_A3 == ISO_country,]
# Affichage
plot(st_geometry(zoom_country))

# Découpage du raster en fonction des frontières du MALI
crop_countrie <- crop(my_data_raster, zoom_country)
# Affichage
plot(crop_countrie)


# Construction d'une boucle pour créer une carte par mois
maxi <- max(minmax(crop_countrie))

for (i in 1:nlyr(my_data_raster)){

  plot(crop_countrie[[i]], 
       main = paste(dates2[i], " - ",  var_name$value  , " - ", zoom_country$NAME_EN), 
       type="continuous", 
       range=c(1,maxi)) 
  
  plot(countries, 
       lwd = 0.4,
       col = NA, 
       border = "grey30", 
       add = TRUE)
}




##########-------- Graphique évolution précipitation moyenne ---------##########

# Calcul des précipitation moyenne de chaque mois pour le MALI
average_all_years <- extract(my_data_raster, vect(zoom_country), mean, na.rm=TRUE)

# Mise en forme du résultat en tableau
average_all_years <- as.data.frame(t(average_all_years[,-1]))
average_all_years$date <- factor(dates2, levels = dates2)


# Graphique 
library(ggplot2)
ggplot(data=average_all_years, aes(x=date, y=V1, group=1)) +
  geom_line(color="blue4")+
  geom_point() + 
  xlab("") + 
  ylab(dunits$value) + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  ggtitle(paste0(var_name$value, " - Monthly average - ", zoom_country$NAME_EN))






##########------ Moyenne précipitations nationale (sur un mois) ------##########

# Mois 1
mois <- 1

average <- extract(my_data_raster[[mois]], vect(countries), mean, na.rm=TRUE)
countries$value <- average[,2]

# Visualisation exploratoire interactive
library(mapview)
mapview(countries)
mapview(countries["value"])






##########------------- Cartographie thématique finalisée ------------##########

sf::sf_use_s2(FALSE) 

library(mapsf)
mf_map(x = countries,
       var = "value",
       type = "choro", 
       breaks = "quantile",
       nbreaks = 6,
       pal = "Blues 3",
       leg_pos = "left",
       border = "grey60",
       lwd = 0.5,
       leg_title = paste0(var_name$value, "\n", dunits$value),  
       leg_val_rnd = 0)

mf_title(paste0("Monthly average - ", var_name$value, " - ", dates2[mois]))
mf_credits(paste0("Auteurs : Hugues pecout, 2023\nSources : ", institution[2]))
mf_annotation(x = st_centroid(st_as_sf(zoom_country)), 
              txt = zoom_country$NAME_EN, 
              halo = TRUE, 
              pos = "topleft",
              cex = 0.8)
