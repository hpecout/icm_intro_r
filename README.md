# Analyse de données avec R

## Utilisation d’un langage de programmation Open Source pour les SHS

### Formation « Données climatiques et données migratoires », l'Institut Convergences Migrations, Novembre 2023


&rarr; Consulter le [**diaporama**](https://icm-intro-r-hpecout-2f8b698b44c8fcbba51fc31df918c3ac1311a83e670.gitpages.huma-num.fr/#/title-slide)
